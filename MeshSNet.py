import keras
import numpy as np
import tensorflow as tf

from keras.models import Model
from keras import backend as K

from keras.layers import Input, Flatten
from keras.layers.convolutional import Conv1D, UpSampling1D
from keras.layers.pooling import MaxPooling1D
from keras.layers.merge import Concatenate, Dot
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Activation, Dense, Dropout, Reshape


def fmeasure_loss(alpha=1):
    '''
    F_{beta}-Measure loss (when beta==1 --> Dice loss)
    alpha = beta**2
    '''

    def fmeasure_loss_fixed(y_true, y_pred):
        y_true = K.flatten(y_true)
        y_pred = K.flatten(y_pred)

        return 1 - ((1.0 + alpha) * K.sum(y_true * y_pred)
                    + K.epsilon()) / (alpha * K.sum(y_true)
                                      + K.sum(y_pred) + K.epsilon())

    return fmeasure_loss_fixed


def sen(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)

    tp = K.sum(y_pred_f * y_true_f)
    re = (tp + K.epsilon()) / (K.sum(y_true_f) + K.epsilon())

    return re


def ppv(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)

    tp = K.sum(y_pred_f * y_true_f)
    re = (tp + K.epsilon()) / (K.sum(y_pred_f) + K.epsilon())

    return re


def dsc(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)

    tp = K.sum(y_pred_f * y_true_f)
    re = (2 * tp + K.epsilon()) / (K.sum(y_true_f) + K.sum(y_pred_f) + K.epsilon())

    return re


def wdsc(y_true, y_pred):
    num_cls = K.int_shape(y_pred)[-1]
    y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
    y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())

    dscs = 0.0
    for i in range(num_cls - 1):
        dscs += dsc(y_true[:, :, i], y_pred[:, :, i])
    dscs += 0.8 * dsc(y_true[:, :, -1], y_pred[:, :, -1])
    return dscs / num_cls


def mdsc(y_true, y_pred):
    num_cls = K.int_shape(y_pred)[-1]
    dscs = 0.0
    for i in range(num_cls - 1):
        dscs += dsc(y_true[:, :, i], y_pred[:, :, i])
    return dscs / (num_cls - 1)


def msen(y_true, y_pred):
    num_cls = K.int_shape(y_pred)[-1]
    sens = 0.0
    for i in range(num_cls - 1):
        sens += sen(y_true[:, :, i], y_pred[:, :, i])
    return sens / (num_cls - 1)


def mppv(y_true, y_pred):
    num_cls = K.int_shape(y_pred)[-1]
    ppvs = 0.0
    for i in range(num_cls - 1):
        ppvs += ppv(y_true[:, :, i], y_pred[:, :, i])
    return ppvs / (num_cls - 1)


def generalized_dice_loss(y_true, y_pred):
    num_clses = K.int_shape(y_pred)[-1]

    num_faces = K.int_shape(y_pred)[1]
    num_faces_per_cls = K.sum(y_true, axis=1, keepdims=True)
    weights = 1 / (K.pow(num_faces_per_cls, 1.5))  # K.square(num_faces_per_cls)
    weights = UpSampling1D(size=num_faces)(weights)

    alpha = 1.0
    # alpha = 1.0 * weights
    # alpha[:,:,0:-1] = 0.8
    # alpha[:,:,-1] = 1
    tp = K.sum((1 + alpha) * weights * y_true * y_pred, axis=[1, -1])
    domi = K.sum(weights * (alpha * y_true + y_pred), axis=[1, -1])

    return 1 - tp / domi


def wce(y_true, y_pred):
    num_faces = K.int_shape(y_pred)[1]
    num_faces_per_cls = K.sum(y_true, axis=1, keepdims=True)
    weights = 1 / (K.pow(num_faces_per_cls, 1.5))  # K.square(num_faces_per_cls)
    weights = UpSampling1D(size=num_faces)(weights)

    y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
    y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())

    loss = y_true * K.log(y_pred)
    loss = -K.mean(K.sum(K.sum(loss, -1), -1))

    return loss


def focal_loss(gamma=2):
    '''
    Focal loss (Lin et al., "Focal Loss for Object Detection", ICCV 2017)
    '''

    def focal_loss_fixed(y_true, y_pred):
        num_faces = K.int_shape(y_pred)[1]
        num_faces_per_cls = K.sum(y_true, axis=1, keepdims=True)
        weights = 1 / (num_faces_per_cls)  # K.square(num_faces_per_cls)
        weights = UpSampling1D(size=num_faces)(weights)

        y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())

        loss = y_true * K.pow((1 - y_pred), gamma) * K.log(y_pred)
        loss = -K.mean(K.sum(K.sum(loss, -1), -1))
        return loss

    return focal_loss_fixed


def wfl(y_true, y_pred):
    num_cls = K.int_shape(y_pred)[-1]
    y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
    y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())

    fls = 0.0
    for i in range(num_cls - 1):
        fls += K.sum(y_true[:, :, i] * K.pow((1 - y_pred[:, :, i]), 1) * K.log(y_pred[:, :, i]))
    fls += 0.8 * K.sum(y_true[:, :, -1] * K.pow((1 - y_pred[:, :, -1]), 1) * K.log(y_pred[:, :, -1]))
    return -K.mean(fls)


def mean_dice_loss(y_true, y_pred):
    return 1 - wdsc(y_true, y_pred)


def hybloss(y_true, y_pred):
    return 1e-5 * wce(y_true, y_pred) + 1.0 * mean_dice_loss(y_true, y_pred)


class MeshSNet(object):

    def __init__(self, num_faces, num_feats, num_clses,
                 with_bn=True, with_dropout=True, drop_prob=0.5):
        self.N = num_faces
        self.M = num_feats
        self.C = num_clses
        self.with_bn = with_bn
        self.with_dropout = with_dropout
        if with_dropout:
            self.drop_prob = drop_prob

    def seg_net(self, loss=mean_dice_loss,
                metrics=[mdsc, msen, mppv], optimizer='Adam'):

        x = Input((self.N, self.M), name='input_faces')
        am_s1 = Input((self.N, self.N), name='adjacency_scale1')
        am_s2 = Input((self.N, self.N), name='adjacency_scale2')

        ''' encoding path '''
        # mlp 1
        mlp1_f = self.mlp(x, counter=1, num_layers=2,
                          num_chnls=[64, 64],
                          drop_flag=[False, False])

        # feature transformation/alignment
        ftn_mat = self.tnet(mlp1_f, 64, idx='ftn')
        mlp1_f = Dot(axes=-1, name='ftn_ot')([mlp1_f, ftn_mat])

        # contextual module 1
        mlp1_cf = Dot(axes=-2, name='context_pool1')([am_s1, mlp1_f])
        CM1Trans1 = Conv1D(filters=32, kernel_size=1, name='cm1_transfer1')
        mlp1_cf = CM1Trans1(mlp1_cf)
        mlp1_ft = CM1Trans1(mlp1_f)
        if self.with_bn:
            mlp1_cf = BatchNormalization(axis=-1)(mlp1_cf)
            mlp1_ft = BatchNormalization(axis=-1)(mlp1_ft)
        mlp1_cf = Activation('relu')(mlp1_cf)
        mlp1_ft = Activation('relu')(mlp1_ft)
        mlp1_cf = Concatenate(axis=-1, name='cm1_merge')([mlp1_ft, mlp1_cf])
        mlp1_cf = Conv1D(filters=64, kernel_size=1, name='cm1_transfer2')(mlp1_cf)
        if self.with_bn:
            mlp1_cf = BatchNormalization(axis=-1)(mlp1_cf)
        mlp1_cf = Activation('relu')(mlp1_cf)

        # mlp 2
        mlp2_f = self.mlp(mlp1_cf, counter=2, num_layers=3,
                          num_chnls=[64, 128, 512],
                          drop_flag=[False, False, True])

        # contextual module 2
        mlp2_cf1 = Dot(axes=-2, name='context_pool2')([am_s1, mlp2_f])
        mlp2_cf2 = Dot(axes=-2, name='context_pool3')([am_s2, mlp2_f])
        CM2Trans1 = Conv1D(filters=128, kernel_size=1, name='cm2_transfer1')
        mlp2_cf1 = CM2Trans1(mlp2_cf1)
        mlp2_cf2 = CM2Trans1(mlp2_cf2)
        mlp2_ft = CM2Trans1(mlp2_f)
        if self.with_bn:
            mlp2_cf1 = BatchNormalization(axis=-1)(mlp2_cf1)
            mlp2_cf2 = BatchNormalization(axis=-1)(mlp2_cf2)
            mlp2_ft = BatchNormalization(axis=-1)(mlp2_ft)
        mlp2_cf1 = Activation('relu')(mlp2_cf1)
        mlp2_cf2 = Activation('relu')(mlp2_cf2)
        mlp2_ft = Activation('relu')(mlp2_ft)
        mlp2_cf = Concatenate(axis=-1, name='cm2_merge')([mlp2_ft, mlp2_cf1, mlp2_cf2])
        mlp2_cf = Conv1D(filters=512, kernel_size=1, name='cm2_transfer2')(mlp2_cf)
        if self.with_bn:
            mlp2_cf = BatchNormalization(axis=-1)(mlp2_cf)
        mlp2_cf = Activation('relu')(mlp2_cf)

        # global feature
        gf = MaxPooling1D(pool_size=self.N, name='global_feature')(mlp2_cf)

        ''' decoding path '''
        up_gf = UpSampling1D(size=self.N, name='up_global_feature')(gf)

        # skip connection
        merge_f = Concatenate(axis=-1, name='skip_connect')([mlp1_f, mlp1_cf, mlp2_f, mlp2_cf, up_gf])
        #merge_f = Concatenate(axis=-1, name='skip_connect')([mlp1_cf, up_gf])

        mlp3_f = self.mlp(merge_f, counter=3, num_layers=3,
                          num_chnls=[256, 256, 128],
                          drop_flag=[False, False, True])

        y = self.mlp_ot(mlp3_f, num_chnls=128)

        net = Model(inputs=[x,am_s1,am_s2], outputs=y)

        optimizer = keras.optimizers.Adam(lr=0.01, beta_1=0.9, beta_2=0.999,
                                          epsilon=1e-8, decay=1e-5, amsgrad=True)  #
        net.compile(loss=loss, metrics=metrics, optimizer=optimizer)

        return net


    def mlp(self, x, counter=1, num_layers=2, num_chnls=[64, 64], drop_flag=[False, False]):
        ''' multi-layer perceptron '''

        for i_layer in range(num_layers):
            x = Conv1D(filters=num_chnls[i_layer], kernel_size=1,
                       name='mlp{0}_layer{1}'.format(counter, i_layer + 1))(x)
            if self.with_bn:
                x = BatchNormalization(axis=-1)(x)
            x = Activation('relu')(x)
            if drop_flag[i_layer]:
                x = Dropout(self.drop_prob)(x)
        return x

    def mlp_ot(self, x, num_chnls=128):

        x = Conv1D(filters=num_chnls, kernel_size=1, name='ot_layer1')(x)
        if self.with_bn:
            x = BatchNormalization(axis=-1)(x)
        x = Activation('relu')(x)
        '''
        if self.with_dropout:
            x = Dropout(self.drop_prob)(x)
        '''
        x = Conv1D(filters=self.C, kernel_size=1, name='ot_layer2')(x)
        x = Activation('softmax', name='seg')(x)

        return x

    def tnet(self, x, num_chnls, idx='stn'):
        ''' TNet for spatial/feature transformation/alignment '''

        f = Conv1D(filters=64, kernel_size=1, name=idx + '_conv1',
                   input_shape=(self.N, num_chnls))(x)
        if self.with_bn:
            f = BatchNormalization(axis=-1)(f)
        f = Activation('relu')(f)

        f = Conv1D(filters=128, kernel_size=1, name=idx + '_conv2')(f)
        if self.with_bn:
            f = BatchNormalization(axis=-1)(f)
        f = Activation('relu')(f)

        f = Conv1D(filters=512, kernel_size=1, name=idx + '_conv3')(f)
        if self.with_bn:
            f = BatchNormalization(axis=-1)(f)
        f = Activation('relu')(f)

        f = MaxPooling1D(pool_size=self.N, name=idx + '_pool')(f)

        f = Conv1D(filters=256, kernel_size=1, name=idx + '_conv4')(f)
        if self.with_bn:
            f = BatchNormalization(axis=-1)(f)
        f = Activation('relu')(f)

        f = Conv1D(filters=128, kernel_size=1, name=idx + '_conv5')(f)
        if self.with_bn:
            f = BatchNormalization(axis=-1)(f)
        f = Activation('relu')(f)

        # initialize as identity transformation
        t = Conv1D(filters=num_chnls ** 2, kernel_size=1,
                   weights=[np.zeros([1, 128, num_chnls ** 2]),
                            np.eye(num_chnls, dtype='float32').flatten()],
                   name=idx + '_mat_flat')(f)

        t = Reshape((num_chnls, num_chnls), name=idx + '_mat')(t)

        return t


if __name__ == '__main__':
    net = MeshSNet(5000, 15, 15).seg_net()

    net.summary()
