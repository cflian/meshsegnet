import numpy as np
import os
from scipy import ndimage as nd
import scipy.io as sio
import random
import h5py
import glob
import keras

from MeshSNet import MeshSNet
from stl_to_vtk import stl_to_vtk
from scipy.spatial import distance_matrix

if __name__ == '__main__':

    model_path = "saved_model/MeshSegNet_Ablation_Results/"
    model_name = 'meshsnet_xyza_5fcv_iter3.best.h5'

    h5_path = '/home/cflian/Data/maxillary_surfaces_original_h5/'
    stl_path = '/home/cflian/Data/re_output/' #'/home/cflian/Data/maxillary_surfaces_T0_np9999-1/'

    vtk_path = '/home/cflian/Data/maxillary_surfaces_vtk_MeshSegNet_xyza_5cv/'
    if not os.path.exists(vtk_path):
        os.makedirs(vtk_path)

    num_inputs, num_outputs = 15, 15  # input features, output labels

    idx = '036'
    val_data_list = glob.glob(h5_path + '*_'+idx+'.h5')

    dsc = np.zeros((len(val_data_list), 14), dtype='float32')
    sen = np.zeros((len(val_data_list), 14), dtype='float32')
    ppv = np.zeros((len(val_data_list), 14), dtype='float32')
    for i_subj in range(len(val_data_list)):
        print val_data_list[i_subj]
        h5f = h5py.File(val_data_list[i_subj], 'r')
        X = np.array(h5f.get('faces'))
        gt = np.array(h5f.get('labels'))
        h5f.close()

        X = (X - np.ones((X.shape[0], 1)) * np.mean(X, axis=0)) / (np.ones((X.shape[0], 1)) * np.std(X, axis=0))
        num_faces = X.shape[0]

        D = distance_matrix(X[:, 9:12], X[:, 9:12])
        S1 = np.zeros((num_faces, num_faces), dtype='float32')
        S2 = np.zeros((num_faces, num_faces), dtype='float32')
        S1[D < 0.1] = 1.0
        S2[D < 0.2] = 1.0
        S1 = S1 / np.dot(np.sum(S1, axis=1, keepdims=True),
                         np.ones((1, num_faces)))
        S2 = S2 / np.dot(np.sum(S2, axis=1, keepdims=True),
                         np.ones((1, num_faces)))
        S1 = S1.astype('float32')
        S2 = S2.astype('float32')

        net = MeshSNet(num_faces=num_faces, num_feats=num_inputs,
                       num_clses=num_outputs, with_bn=True,
                       with_dropout=True, drop_prob=0.5).seg_net()
        net.load_weights(model_path + model_name)

        inputs = np.zeros((1, num_faces, num_inputs), dtype='float32')
        inputs[0, :, 0:9] = X[:, 0:9]
        inputs[0, :, 9:12] = X[:, 9:12]
        amat_s1 = np.zeros((1, num_faces, num_faces), dtype='float32')
        amat_s2 = np.zeros((1, num_faces, num_faces), dtype='float32')
        amat_s1[0, :, :] = S1
        amat_s2[0, :, :] = S2
        outputs = net.predict([inputs,amat_s1,amat_s2])#

        probs = outputs[0, :]

        max_idxs = np.argmax(probs, axis=1)

        crisp = np.zeros(probs.shape, dtype='float32')
        for i in range(probs.shape[0]):
            crisp[i, max_idxs[i]] = 1.0

        dsc[i_subj, :] = np.sum(2.0 * crisp[:, :-1] * gt[:, :-1], axis=0) / (
                    np.sum(crisp[:, :-1], axis=0) + np.sum(gt[:, :-1], axis=0))
        sen[i_subj, :] = np.sum(1.0 * crisp[:, :-1] * gt[:, :-1], axis=0) / np.sum(gt[:, :-1], axis=0)
        ppv[i_subj, :] = np.sum(1.0 * crisp[:, :-1] * gt[:, :-1], axis=0) / np.sum(crisp[:, :-1], axis=0)
        print('DSC = {0}; SEN = {1}; PPV = {2}.'.format(np.mean(dsc[i_subj, :]), np.mean(sen[i_subj, :]),
                                                        np.mean(ppv[i_subj, :])))

        # save results as subj_name.vtk

        subj_name = val_data_list[i_subj].replace(h5_path, '').replace('.h5', '').replace('A0_', '')
        stl_to_vtk(stl_path, vtk_path, subj_name, ['T2', 'T3', 'T4', 'T5', 'T6',
                                                   'T7', 'T8', 'T9', 'T10', 'T11',
                                                   'T12', 'T13', 'T14', 'T15'], probs)

        sio.savemat(vtk_path + 'msn_' + subj_name + '.mat',
                    {'prob': probs, 'seg': crisp, 'lab': max_idxs, 'gt': gt})

    sio.savemat(vtk_path + 'MeshSegNet_xyza_5cv_detailed_S'+idx+'.mat',
                {'dsc': dsc, 'sen': sen, 'ppv': ppv})
