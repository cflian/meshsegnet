import numpy as np
import os
from scipy import ndimage as nd
import scipy.io as sio
import random
import h5py
import glob
import keras

from scipy.spatial import distance_matrix

def data_flow(data_list, num_faces, num_inputs,
              num_outputs, batch_size, train_flag=True):
    input_shape = (batch_size, num_faces, num_inputs)
    output_shape = (batch_size, num_faces, num_outputs)
    adjacent_shape = (batch_size, num_faces, num_faces)

    inputs = np.zeros(input_shape, dtype='float32')
    outputs = np.zeros(output_shape, dtype='float32')
    amat_s1 = np.zeros(adjacent_shape, dtype='float32')
    amat_s2 = np.zeros(adjacent_shape, dtype='float32')

    i_batch = 0
    data_idxs = range(len(data_list))
    while True:
        if train_flag:
            random.shuffle(data_idxs)

        for i_iter in data_idxs:
            h5f = h5py.File(data_list[i_iter], 'r')
            X = np.array(h5f.get('faces'))
            Y = np.array(h5f.get('labels'))
            h5f.close()

            X = (X - np.ones((X.shape[0], 1)) * np.mean(X, axis=0)) / (np.ones((X.shape[0], 1)) * np.std(X, axis=0))

            # potential augmentation
            num_positives = sum(Y[:, 0:-1] == 1)
            if train_flag:
                # num_positives = [np.ceil(np.random.uniform(0.45,0.55)*num_positives[i]) for i in range(num_outputs-1)]
                num_positives = np.ceil(0.5 * num_positives)

            positive_idxs = [np.where(Y[:, i] == 1)[0] for i in range(num_outputs - 1)]
            num_negatives = num_faces - int(sum(num_positives))
            negative_idxs = np.where(Y[:, -1] == 1)[0]

            if train_flag:
                positive_idxs = [np.random.choice(positive_idxs[i],
                                                  size=int(num_positives[i]),
                                                  replace=False)
                                 for i in range(Y.shape[1] - 1)]
                negative_idxs = np.random.choice(negative_idxs, size=num_negatives, replace=False)
            else:
                '''
                negative_idxs = negative_idxs[0: negative_idxs.size:
                                              int(np.trunc(1.0*negative_idxs.size/num_negatives))]
                negative_idxs = negative_idxs[0: num_negatives]
                '''
                negative_idxs = np.random.choice(negative_idxs, size=num_negatives, replace=False)

            sum_positive_idxs = []
            for i in range(num_outputs - 1):
                sum_positive_idxs += positive_idxs[i].tolist()

            sample_idxs = sum_positive_idxs + negative_idxs.tolist()
            sample_idxs.sort()
            X = X[sample_idxs, :]
            Y = Y[sample_idxs, :]

            inputs[i_batch, :] = X
            outputs[i_batch, :] = Y
            
            D = distance_matrix(X[:,9:12], X[:,9:12])
            S1 = np.zeros((num_faces,num_faces), dtype='float32')
            S2 = np.zeros((num_faces,num_faces), dtype='float32')
            S1[D<0.1] = 1.0
            S2[D<0.2] = 1.0
            S1 = S1 / np.dot(np.sum(S1,axis=1,keepdims=True),
                             np.ones((1,num_faces)))
            S2 = S2 / np.dot(np.sum(S2,axis=1,keepdims=True),
                             np.ones((1,num_faces)))
            amat_s1[i_batch,:] = S1
            amat_s2[i_batch,:] = S2

            i_batch += 1

            if i_batch == batch_size:
                yield ([inputs, amat_s1, amat_s2], outputs)
                inputs = np.zeros(input_shape, dtype='float32')
                outputs = np.zeros(output_shape, dtype='float32')
                amat_s1 = np.zeros(adjacent_shape, dtype='float32')
                amat_s2 = np.zeros(adjacent_shape, dtype='float32')
                i_batch = 0


if __name__ == '__main__':

    from MeshSNet import MeshSNet

    trn_data_path = '/home/cflian/Data/maxillary_surfaces2_h5/'
    trn_data_list = glob.glob(trn_data_path + '*.h5')
    tst_data_list = glob.glob(trn_data_path + '*_010.h5')
    trn_data_list = list(set(trn_data_list) - set(tst_data_list))

    val_data_list = glob.glob(trn_data_path + '*_034.h5')

    trn_data_list = list(set(trn_data_list) - set(val_data_list))

    num_faces = 6000  # number of triangles/faces/cells
    num_inputs, num_outputs = 15, 15  # input features, output labels

    trn_batch_size, num_epochs = 10, 200
    val_batch_size = 10

    trn_steps = int(np.round(len(trn_data_list) / trn_batch_size))
    val_steps = int(np.round(len(val_data_list) / val_batch_size))

    net = MeshSNet(num_faces=num_faces, num_feats=num_inputs,
                   num_clses=num_outputs, with_bn=True,
                   with_dropout=True, drop_prob=0.5).seg_net()
    net.summary()

    model_path = "saved_model/"
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    model_name = 'meshsnet.best.h5'

    history_name = 'meshsnet_history.mat'

    trn_flow = data_flow(trn_data_list, num_faces, num_inputs,
                         num_outputs, trn_batch_size, train_flag=True)

    val_flow = data_flow(val_data_list, num_faces, num_inputs,
                         num_outputs, val_batch_size, train_flag=False)

    checkpoint = keras.callbacks.ModelCheckpoint(filepath=model_path + model_name,
                                                 save_best_only=True, monitor='val_mdsc', mode='max')
    history = keras.callbacks.History()

    net.fit_generator(generator=trn_flow, steps_per_epoch=trn_steps,
                      epochs=num_epochs, validation_data=val_flow, max_queue_size=15,
                      validation_steps=val_steps, callbacks=[checkpoint, history])

    sio.savemat(model_path + history_name, {'train_history': history.history})

