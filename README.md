# README #

The model was implemented with Keras + Tensorflow backend.
Please note that the implementeation was not optimized... So please feel free to make any modifications when you need to do...


If you use this package, please kindly cite the related papers:

[1] Lian et al., "Deep Multi-Scale Mesh Feature Learning for Automated Labeling of Raw Dental Surfaces from 3D Intraoral Scanners", IEEE-TMI, 2019.

[2] Lian et al., "MeshSNet: Deep Multi-scale Mesh Feature Learning for End-to-End Tooth Labeling on 3D Dental Surfaces", MICCAI, 2019. 
