# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 15:42:59 2018

@author: chlian
"""

import vtk
import numpy as np
import os

from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk

from stl import mesh


def stl_to_vtk(stl_path, vtk_path, subj_name, gt_list, teeth_probs=None):
    
    stlReader = vtk.vtkSTLReader()
    stlReader.SetFileName(stl_path+subj_name+'.stl')
    stlReader.Update()
    points_vtk = stlReader.GetOutput().GetPoints().GetData()
    points_vtk = vtk_to_numpy(points_vtk)
    polys_vtk = stlReader.GetOutput().GetPolys().GetData()
    polys_vtk = vtk_to_numpy(polys_vtk)
    polys_vtk = np.reshape(polys_vtk,(-1,4))

    mset = mesh.Mesh.from_file(stl_path+subj_name+'.stl')
    faces = mset.points
    teeth_true = []
    num_teeth = len(gt_list)
    for i_tooth in range(num_teeth):
        mset_lbl = mesh.Mesh.from_file(stl_path+subj_name+'_'
                                        +gt_list[i_tooth]+'.stl')
        lbl_faces = mset_lbl.points
        lbl_idxs = [np.where(np.all(faces==lbl_faces[i,:],axis=1))[0][0] 
                for i in range(lbl_faces.shape[0])]
        tooth_label = np.zeros((faces.shape[0],1), dtype='float32')
        tooth_label[lbl_idxs] = 1
        teeth_true.append(tooth_label)

    #points_vtk = np.reshape(points_vtk,(-1,9))
    f = open(vtk_path+subj_name+'.vtk', 'w')
    f.write('# vtk DataFile Version 4.2 \n')
    f.write('vtk output \n')
    f.write('ASCII \n')
    f.write('DATASET POLYDATA \n')
    f.write('POINTS ' + str(points_vtk.shape[0]) + ' float\n')
    f.write('\n'.join(' '.join('%f' %x for x in y) for y in points_vtk))
    f.write('\n')
    f.write('\n')
    f.write('POLYGONS ' + str(faces.shape[0]) + ' ' + str(faces.shape[0]*4) + '\n')
    f.write('\n'.join(' '.join('%d' %x for x in y) for y in polys_vtk))
    f.write('\n')
    f.write('\n')

    f.write('CELL_DATA ' + str(faces.shape[0]) + '\n')
    for i_tooth in range(num_teeth):
        f.write('SCALARS true_' + gt_list[i_tooth] + ' int\n')
        f.write('LOOKUP_TABLE true_'+gt_list[i_tooth]+'Table\n')
        f.write('\n'.join(' '.join('%d' %x for x in y) for y in teeth_true[i_tooth]))
        f.write('\n')
        f.write('\n')
        
    if num_teeth > 1:
        teeth_true = np.column_stack(teeth_true)
        teeth_true = np.column_stack([teeth_true, 1-np.sum(teeth_true, axis=1)])
        teeth_true = np.argmax(teeth_true, axis=1) + 1
        teeth_true = np.reshape(teeth_true, (-1,1))
        f.write('SCALARS true_ALL int\n')
        f.write('LOOKUP_TABLE true_ALLTable\n')
        f.write('\n'.join(' '.join('%d' %x for x in y) for y in teeth_true))
        f.write('\n')
        f.write('\n')
    
    if teeth_probs is not None:
        for i_tooth in range(num_teeth):
            f.write('SCALARS prob_' + gt_list[i_tooth] + ' float\n')
            f.write('LOOKUP_TABLE prob_'+gt_list[i_tooth]+'Table\n')
            f.write('\n'.join(' '.join('%f' %x for x in y) for y in teeth_probs[:,i_tooth:i_tooth+1]))
            f.write('\n')
            f.write('\n')
            
        ''' Note: following part should be refined for multi-class! '''    
        for i_tooth in range(num_teeth):
            teeth_pred = np.zeros((faces.shape[0],1), dtype='int8')
            teeth_pred[teeth_probs[:,i_tooth]>=0.5] = 1
            teeth_pred[teeth_probs[:,i_tooth]<0.5] = 0
            f.write('SCALARS pred_' + gt_list[i_tooth] + ' int\n')
            f.write('LOOKUP_TABLE pred_'+gt_list[i_tooth]+'Table\n')
            f.write('\n'.join(' '.join('%d' %x for x in y) for y in teeth_pred))
            f.write('\n')
            f.write('\n')
            
        if num_teeth > 1:
            teeth_pred = np.argmax(teeth_probs, axis=1) + 1
            teeth_pred = np.reshape(teeth_pred, (-1,1))
            f.write('SCALARS pred_ALL int\n')
            f.write('LOOKUP_TABLE pred_ALLTable\n')
            f.write('\n'.join(' '.join('%d' %x for x in y) for y in teeth_pred))
            f.write('\n')
            f.write('\n')
            
    f.close()
    
    
    
if __name__ == '__main__':
    

    stl_path = '/Users/chlian/Documents/Datasets/maxillary_surfaces_stl/'
    subj_name = 'A1_Sample_01'
    vtk_path = '/Users/chlian/Documents/Datasets/DS_Surface/'
    stl_to_vtk(stl_path, vtk_path, subj_name, ['T08'])
